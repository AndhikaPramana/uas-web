<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardControllers;
use App\Http\Controllers\LoginControllers;
use App\Http\Controllers\ZoomController;
use App\Http\Controllers\PinjamController;
use App\Http\Controllers\LayananController;
use App\Http\Controllers\JadwalController;
use App\Http\Controllers\DetailController;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pengguna.login');
});

//middleware
Route::group(['middleware' => ['auth', 'ceklevel:admin,user']], function () {
    Route::get('/dashboard', [DashboardControllers::class, 'index'])->name('dashboard');
});

// login, sing up
Route::get('/login', [LoginControllers::class, 'login'])->name('login');
Route::get('/registrasi', [LoginControllers::class, 'registrasi'])->name('registrasi');
Route::post('/simpanregistrasi', [LoginControllers::class, 'simpanregistrasi'])->name('simpanregistrasi');
Route::post('/postlogin', [LoginControllers::class, 'postlogin'])->name('postlogin');
Route::get('/logout', [LoginControllers::class, 'logout'])->name('logout');

//admin
Route::group(['middleware' => ['auth', 'ceklevel:admin']], function () {

    Route::get('/admin', [DashboardControllers::class, 'admin'])->name('admin');
    Route::get('/layanan', [LayananController::class, 'index'])->name('layanan');
    Route::match(['get', 'post'], '/create/{id}', [LayananController::class, 'create'])->name('layananreq');
    Route::get('/hapus/{id}', [LayananController::class, 'hapus'])->name('hapus');
    Route::match(['get', 'post'], '/reject/{id}', [LayananController::class, 'reject'])->name('layananreqreject');
    Route::get('/deleter/{id}', [LayananController::class, 'deleter'])->name('deleter');
    Route::get('/jadwal', [JadwalController::class, 'index'])->name('jadwal');
});

//user
Route::group(['middleware' => ['auth', 'ceklevel:admin,user']], function () {

    Route::get('/dashboard', [DashboardControllers::class, 'index'])->name('Dashboard');
    Route::get('/user', [DashboardControllers::class, 'user'])->name('user');
});

Route::group(['middleware' => ['auth', 'ceklevel:user']], function () {

    Route::get('/pinjam', [PinjamController::class, 'index'])->name('pinjam');
    Route::post('/pinjam', [PinjamController::class, 'postRequest'])->name('postRequest');
    Route::get('/post_req', [PinjamController::class, 'indexSecond'])->name('indexSecond');
    Route::match(['get', 'post'], '/post_req/{id}', [PinjamController::class, 'updateKembali'])->name('updateKembali');
    Route::get('/deleting/{id}', [PinjamController::class, 'destroy'])->name('deleting');
    Route::get('/detail', [DetailController::class, 'index'])->name('detail');
});

//forget
Route::get('/forget-password', [LoginControllers::class, 'getforgetpassword'])->name('getforgetpassword');
Route::post('/forget-password', [LoginControllers::class, 'postforgetpassword'])->name('postforgetpassword');

//reset
Route::get('/reset-password/{reset_code}', [LoginControllers::class, 'getResetPassword'])->name('getResetPassword');
Route::post('/reset-password/{reset_code}', [LoginControllers::class, 'postResetPassword'])->name('postResetPassword');

//zoom
Route::get('/listzoom', [ZoomController::class, 'index'])->name('zoomlist');
Route::get('/createzoom', [ZoomController::class, 'create'])->name('createzoom');
Route::post('/insertzoom', [ZoomController::class, 'store'])->name('insertzoom');
Route::get('/editzoom/{id}', [ZoomController::class, 'edit'])->name('editzoom');
Route::post('/updatezoom/{id}', [ZoomController::class, 'update'])->name('updatezoom');
Route::get('/deletezoom/{id}', [ZoomController::class, 'destroy'])->name('detelezoom');



















// Route::get('/zoomlist', [ZoomController::class, 'zoomlist'])->name('zoomlist');
// Route::get('/manage_jadwal', [DashboardControllers::class, 'manage_jadwal'])->name('manage_jadwal');
// Route::get('/manage_peminjaman', [DashboardControllers::class, 'manage_peminjaman'])->name('manage_peminjaman');

// // peminjaman
// // Route::get('/ajukan_peminjaman', [DashboardControllers::class, 'ajukan_peminjaman'])->name('ajukan_peminjaman');
