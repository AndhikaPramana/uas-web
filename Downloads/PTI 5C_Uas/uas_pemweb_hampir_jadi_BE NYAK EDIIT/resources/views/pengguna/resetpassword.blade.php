
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-ZOOM</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/sb-admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('vendor/sb-admin/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="bg-gradient-primary">

  <div class="container">

      <!-- Outer Row -->
      <div class="row justify-content-center">

          <div class="col-lg-7">

              <div class="card o-hidden border-0 shadow-lg my-5">
                  <div class="card-body p-0">
                      <!-- Nested Row within Card Body -->
                      <div class="row">
                          
                          <div class="col-lg">
                              <div class="p-5">
                                  <div class="text-center">
                                      <h1 class="h4 text-gray-900 mb-4">Reset Password!</h1>
                                  </div>
                                    <form action="{{ route('postResetPassword',$reset_code) }}" class="user" method="POST">
                                    @csrf
                                      <fieldset class="form-label-group form-group position-relative has-icon-left">
                                        <input type="text" name="email" class="form-control" id="email" placeholder="Email" required>
                                        <div class="form-control-position">
                                          <i class="feather icon-mail"></i>
                                        </div>
                                      </fieldset>

                                      <fieldset class="form-label-group position-relative has-icon-left">
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                                        <div class="form-control-position">
                                          <i class="feather icon-lock"></i>
                                        </div>
                                        <label for="password"></label>
                                      </fieldset>

                                      <fieldset class="form-label-group position-relative has-icon-left">
                                      <input id="confirm_password" type="password" onkeyup="check()" class="form-control" name="confirm_password" placeholder="Confirm Password" required>
                                            <span id='message'></span>
                                        <div class="form-control-position">
                                          <i class="feather icon-check"></i>
                                        </div>
                                        <label for="password"></label>
                                      </fieldset>
                                      <!-- <a href="auth-register.html" class="btn btn-outline-primary float-left btn-inline">Register</a> -->
                                      <button type="submit" class="btn btn-primary btn-block">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END: Content-->


  <!-- Bootstrap core JavaScript-->
  <script src="{{ asset('vendor/sb-admin/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ asset('vendor/sb-admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ asset('vendor/sb-admin/js/sb-admin-2.min.js') }}"></script>

  <!-- BEGIN: Page JS-->
  <!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>