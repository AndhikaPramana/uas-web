
<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body id="page-top">
    <div id="wrapper">
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                @include('layouts.topbar')
                <div class="container-fluid">
                    <h1 class="h3 mb-4 text-blue-800 text-primary blockquote text-center">Selamat Datang di Web E-Zoom</h1>

                    @if (auth()->user()->level=="admin")
                        <div class="row">
                            <div class="col-lg-6 mb-4" data-aos="zoom-in">
                                <div class="card shadow mb-4">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><a href="{{ route('layanan') }}">Management Layanan</h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="bi bi-journal-bookmark fa-10x"></i>
                                        </div>
                                       <a>Fitur layanan ini berisikan peminjaman akun oleh user melakukan approve ataupun reject peminjaman serta kemudian Admin dapat membatalkan peminjaman yang di request oleh user serta mengubah status peminjaman ketika akun sudah di kembalikan oleh user.</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 mb-4">
                                <div class="row">
                                    <div class="card shadow mb-4" data-aos="zoom-in">
                                        <div class="card-header py-3">
                                            <h6 class="m-0 font-weight-bold text-primary"><a href="{{ route('jadwal')  }}">Management Jadwal</h6>
                                        </div>
                                        <div class="card-body">
                                            <div class="text-center">
                                                <i class="bi bi-calendar-day fa-10x"></i>
                                            </div>
                                            <a>Fitur jadwal memungkinkan Admin untuk melihat riwayat peminjaman yang sudah di approve oleh Admin dan juga Admin dapat melihat info room zoom yang masih dapat di gunakan oleh user.</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="card shadow mb-4" data-aos="zoom-in">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><a href="{{ route('zoomlist') }}">Management Akun Zoom</h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="bi bi-camera-video-fill fa-10x"></i>
                                        </div>
                                        <a>Fitur akun ini memiliki keluasan pengelolaan aku bagi Admin dimana Admin dapat melakukan penambahan akun zoom, mengubah akun zoom, melihat list dari akun zoom, serta menghapus akun zoom.</a>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        @elseif (auth()->user()->level=="user")
                        <div class="row">
                            <div class="col-lg-6 mb-4">
                                <div class="card shadow mb-4" data-aos="zoom-in">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"><a href="{{ route('indexSecond') }}">Daftar Request Peminjaman Akun Zoom</h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="bi bi-box-arrow-up-right fa-10x"></i>
                                        </div>
                                        <a>Fitur Daftar Request Peminjaman Akun Zoom merupakan fitur bagi user yang difungsikan untuk melihat serta mengakses daftar request peminjaman akun zoom yang dilakukan user atau mahasiswa.</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 mb-4">
                                <div class="card shadow mb-4" data-aos="zoom-in">
                                    <div class="card-header py-3">
                                        <h6 class="m-0 font-weight-bold text-primary"> <a href="{{ route('pinjam')}}">Daftar Akun yang Tersedia</h6>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-center">
                                            <i class="bi bi-card-checklist fa-10x"></i>
                                        </div>
                                        <a>Fitur Daftar Akun yang Tersedia merupakan fitur bagi mahasiswa yang difungsikan untuk melihat daftar akun yang tersedia serta dapat melakukan request akun zoom yang dilakukan oleh user atau mahasiswa.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
           @include('layouts.footer')
        </div>
    </div>
    @include('layouts.button-topbar')
    @include('layouts.logout-model')
    @include('layouts.script')
</body>

</html>
