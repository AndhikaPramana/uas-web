<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

class DashboardControllers extends Controller
{
    function index()
    {
        return view('dashboard');
    }
}
