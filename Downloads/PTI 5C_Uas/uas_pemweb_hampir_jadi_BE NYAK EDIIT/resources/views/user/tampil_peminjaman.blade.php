@extends('layouts.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Daftar Akun Tersedia</h1>
    
<div class="card shadow mb-4">
    
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>ID Akun</th>
                        <th>Nama Akun</th>
                        <th>Kapasitas</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pinjamakun as $list)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td class="id">{{ $list->id }}</td>
                        <td class="nama_akun">{{ $list->nama_akun }}</td>
                        <td class="kapasitas">{{ $list->kapasitas }} Peserta</td>
                        <td class="status_aktif" align="center">
                            @if ($list->status_aktif == 'Aktif')
                            <label class="badge badge-pill badge-success">{{ $list->status_aktif }}</label>
                            @elseif($list->status_aktif == 'Tidak Aktif')
                            <label class="badge badge-pill badge-danger">{{ $list->status_aktif }}</label>
                            @endif
                        </td>
                        <td align="center">
                            @if ($list->status_aktif == 'Aktif')
                            <a href="" style="text-decoration: none; margin-right:10px" data-toggle="modal" data-target="#request-{{ $list->id }}">
                                <button type="button" class="btn m-btn--pill m-btn--air btn-brand m-btn m-btn--customs">
                                    <i class="fa fa-location-arrow"></i>
                                    Request
                                </button>
                            </a>
                            @else
                            <a href="" style="text-decoration: none; margin-right:10px" data-toggle="modal" data-target="#request-{{ $list->id }}">
                                <button disabled type="button" class="btn m-btn--pill m-btn--air btn-brand m-btn m-btn--customs">
                                    <i class="fa fa-location-arrow"></i>
                                    Request
                                </button>
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{-- {-- REQUEST PINJAM --}}
             @foreach ($pinjamakun as $list)
             <div class="modal fade" id="request-{{ $list->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog modal-lg" role="document">
                     <div class="modal-content">
                         <div class="modal-header">
                             <h5 class="modal-title" id="exampleModalLabel">
                                 Request Peminjaman Akun
                             </h5>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">
                                     &times;
                                 </span>
                             </button>
                         </div>
                         <form action="{{ route('postRequest') }}" method="POST">
                             {{ csrf_field() }}
                             <div class="modal-body">
                                 <div class="form-group">
                                     <label for="recipient-name" class="form-control-label">
                                         ID Zoom
                                     </label>
                                     <input type="text" value="{{ $list->id }}" name="id_zoom" class="form-control" id="e_id_zoom">
                                 </div>
 
                                 <div class="form-group">
                                     <label for="recipient-name" class="form-control-label">
                                         Nama Akun
                                     </label>
                                     <input type="text" value="{{$list->nama_akun }}" name="nama_akun" class="form-control" id="e_nama_akun">
                                 </div>
 
                                 <div class="form-group">
                                     <label for="recipient-name" class="form-control-label">
                                         Kapasitas
                                     </label>
                                     <input type="number" value="{{ $list->kapasitas }}" name="kapasitas" class="form-control" id="e_kapasitas">
                                 </div>
 
                                 <div class="form-group">
                                     <label for="recipient-name" class="form-control-label">
                                         Nama Peminjam
                                     </label>
                                     <input required type="text" value="" name="nama_peminjam" class="form-control" id="e_nama_peminjam">
                                 </div>

                                 <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">
                                        Durasi <span class="text-danger" style="font-size:12px">(Format Jam & Menit)</span>
                                    </label>
                                    <input class="form-control m-input" name="durasi" type="time" value="" id="example-time-input">
                                </div>

                                 <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">
                                        Kegiatan
                                    </label>
                                    <input required type="text" value="" name="kegiatan" class="form-control" id="e_nama_peminjam">
                                 </div>
 
                                 <div class="form-group">
                                     <label for="recipient-name" class="form-control-label">
                                         Keterangan
                                     </label>
                                     <textarea required class="form-control" value="" name="keterangan" id="e_keterangan"></textarea>
                                 </div>
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-danger" data-dismiss="modal">
                                     Close
                                 </button>
                                 <button type="submit" class="btn btn-primary">
                                     Request
                                 </button>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>
             @endforeach
             {{-- END REQQUEST PINJAM --}}
        </div>
    </div>
</div>

@endsection