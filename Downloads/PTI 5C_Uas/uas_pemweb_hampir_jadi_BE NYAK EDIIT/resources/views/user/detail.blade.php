@extends('layouts.app')

<script>
    $(document).ready(function() {
        $('#dataTables').DataTable({
            select : true
        });       
    });
</script>
<script>
    $('.deleting').click(function () {
        var deleting = $(this).attr('data-id')
        swal({
            title: "Anda yakin ingin menghapus data ini?",
            text: "Jika iya, data ini akan terhapus secara permanent!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            window.location = "/deleting/"+deleting+""
            swal("Data anda berhasil terhapus secara permanent!", {
            icon: "success",
            });
        } else {
            swal("Data tidak jadi di hapus!");
        }
    });
});
</script>

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Detail</h1>

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>NO</th>                        
                        <th>Nama Akun</th>
                        <th>Nama Peminjam</th>
                        <th>Status Peminjaman</th>
                        <th>Tanggal Peminjaman</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   @foreach ($detail as $list)
                        <tbody>
                            <td>{{ $loop->iteration }}</td>
                            <td class="name">{{ $list->zoom->nama_akun }}</td>
                            <td class="nama_peminjam">{{ $list->nama_peminjam }}</td>
                            <td class="status" align="center">
                                @if ($list->status_pinjam == 'Aprove')
                                <label class="m-badge m-badge--success m-badge--wide">{{ $list->status_pinjam }}</label>
                                @endif
                            </td>
                            <td class="tanggal_pinjam">{{ $list->created_at->format('l, d M Y') }}</td>
                            <td align="center">
                                <a style="text-decoration: none; margin-right:10px" data-toggle="modal" data-target="#detail-{{ $list->id }}">
                                    <button type="button" class="bg-primary" style="border:none; border-radius: 5px;"><i class="fa fa-eye text-light" aria-hidden="true"></i></button>
                                </a>
                            </td>
                        </tbody>
                        @endforeach
                    </table>
                </div>
            </div>
             {{-- LIHAT DATA --}}
             @foreach ($detail as $data)
             <div class="modal fade" id="detail-{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog modal-md" role="document">
                     <div class="modal-content">
                         <div class="modal-header">
                             <h5 class="modal-title" id="exampleModalLabel">
                                 Detail Peminjaman
                             </h5>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true">
                                     &times;
                                 </span>
                             </button>
                         </div>
                         <form action="" method="POST">
                             {{ csrf_field() }}
                             <div class="modal-body">
                                 <div class="row">
                                     <div class="col-md-12">
                                         <table class="table">
                                            <tr>
                                                <th>ID Pinjam</th>
                                                <td>:</td>
                                                <td>{{ $data->id }}</td>
                                            </tr>
                                             <tr>
                                                 <th>ID ZOOM</th>
                                                 <td>:</td>
                                                 <td>{{ $data->akun->id }}</td>
                                             </tr>
                                             <tr>
                                                 <th>Nama Akun</th>
                                                 <td>:</td>
                                                 <td>{{ $data->akun->name }}</td>
                                             </tr>
                                             <tr>
                                                <th>Nama Peminjam</th>
                                                <td>:</td>
                                                <td>{{ $data->nama_peminjam }}</td>
                                            </tr>
                                             <tr>
                                                 <th>Kapasitas</th>
                                                 <td>:</td>
                                                 <td style="margin-left:-20px" >{{ $data->akun->kapasitas }} Orang</td>
                                             </tr>
                                             <tr>
                                                <th>Durasi Pinjam</th>
                                                <td>:</td>
                                                <td style="margin-left:-20px" >{{ $data->durasi }}</td>
                                            </tr>
                                             <tr>
                                                 <th>Tanggal Peminjaman</th>
                                                 <td>:</td>
                                                 <td style="margin-left:-20px" >{{ $data->created_at->format('l, d M Y , h : i : sa') }}</td>
                                             </tr>
                                             <tr>
                                                <th>Tanggal Aprove</th>
                                                <td>:</td>
                                                <td style="margin-left:-20px" >{{ $data->updated_at->format('l, d M Y , h : i : sa') }}</td>
                                            </tr>
                                             <tr>
                                                 <th>Status Peminjaman</th>
                                                 <td>:</td>
                                                 <td>
                                                     @if ($data->status_pinjam == 'Aprove')
                                                         <label class="badge badge-pill badge-success">{{ $data->status_pinjam }}</label>
                                                     @endif
                                                 </td>
                                             </tr>
                                         </table>
                                     </div>
                                 </div>
                             </div>
                             <div class="modal-footer">
                                 <button type="button" class="btn btn-danger" data-dismiss="modal">
                                     Close
                                 </button>
                             </div>
                         </form>
                     </div>
                 </div>
             </div>
             @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection