<?php

namespace App\Http\Controllers;

use App\Models\pinjam;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    function index()
    {
        $detail = pinjam::where('status_pinjam', '!=', 'Request')->get();
        return view('admin.jadwal', ['detail' => $detail]);
    }
}
