@extends('layouts.app')

@section('js_script')
<script>
    $('.deleter').click(function () {
        var deleterAdm = $(this).attr('data-id')
            swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                window.location = "/deleter/" +deleterAdm+ ""
                swal("Poof! Your imaginary file has been deleted!", {
                icon: "success",
                });
            } else {
                swal("Your imaginary file is safe!");
            }
        });
    });
</script>
@endsection

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Management Layanan</h1>

<div class="card shadow mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>ID Akun</th>
                        <th>Nama Akun</th>
                        <th>Nama Peminjam</th>
                        <th>Kapasitas</th>
                        <th>Durasi</th>
                        <th>Status Peminjaman</th>
                        <th>Tanggal Peminjaman</th>
                        <th>Tanggal Kembali</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($dataZoom as $items)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$items->Zoom->id}}</td>                     
                        <td>{{$items->Zoom->nama_akun}}</td>
                        <td>{{$items->nama_peminjam}}</td>
                        <td>{{$items->Zoom->kapasitas}}</td>
                        <td>{{$items->durasi}}</td>
                        <td>{{$items->status_pinjam}}</td>
                        <td>{{$items->created_at->format('l, d M Y') }}</td>
                        <td>{{$items->tanggal_kembali }}</td>
                        <td align="center">
                            @if ($items->status_pinjam == 'Request')
                                <a data-toggle="modal" data-target="#kembali-{{ $items->id }}" style="text-decoration: none; margin-right:10px">
                                    <button class="bg-primary" style="border:none; border-radius: 5px;"><i class="fa fa-check text-light" aria-hidden="true"> Aprove</i></button>
                                </a>
                                <a data-toggle="modal" data-target="#reject-{{ $items->id }}" style="text-decoration: none; margin-right:10px">
                                    <button class="bg-danger text-light px-2 py-1" style="border:none; border-radius: 5px;"><i class="fa fa-user text-light" aria-hidden="true"> Reject</i></button>
                                </a>
                            @elseif ($items->status_pinjam == 'Aprove')    
                                <a data-id="{{ $items->id }}" class="hapus" style="text-decoration: none; margin-right:10px">
                                    <button class="bg-secondary text-light px-2 py-1" style="border:none; border-radius: 5px;"><i class="fa fa-ban text-light" aria-hidden="true"> Batalkan</i></button>
                                </a>
                            @elseif ($items->status_pinjam == 'Reject')    
                                <a data-id="{{ $items->id }}" class="deleter" style="text-decoration: none; margin-right:10px">
                                    <button class="bg-danger text-light px-2 py-1" style="border:none; border-radius: 5px;"><i class="fa fa-trash text-light" aria-hidden="true">Hapus</i></button>
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

  {{-- REQUEST PEMINJAMAN --}}
  @foreach ($dataZoom as $data)
  <div class="modal fade" id="kembali-{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">
                      Aprove Permintaan
                  </h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">
                          &times;
                      </span>
                  </button>
              </div>
              <form action="{{ url('/create/'.$data->id) }}" method="POST">
                  {{ csrf_field() }}
                  <div class="modal-body">
                      <div class="form-group">
                          <label for="recipient-name" class="form-control-label">
                              Status Peminjaman
                          </label>
                          <input type="text" value="Aprove" name="status_pinjam" class="form-control" id="e_status_pinjam">
                      </div>

                      <h5><center>Apakah Anda Yakin Ingin Aprove Permintaan?</center></h5>

                      <center><button type="button" class="btn btn-danger" data-dismiss="modal" style="">
                          Close
                      </button>
                      <button type="submit" class="btn btn-primary">
                          Submit
                      </button></center>
                  </div>
              </form>
          </div>
      </div>
  </div>
@endforeach
{{-- REQUEST PEMINJAMAN --}}

@foreach ($dataZoom as $data)
<div class="modal fade" id="reject-{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Reject Permintaan
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form action="{{ url('/reject/'.$data->id) }}" method="POST">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">
                            Status Peminjaman
                        </label>
                        <input type="text" value="Reject" name="status_pinjam" class="form-control" id="e_status_pinjam">
                    </div>

                    <h5><center>Apakah Anda Yakin, Permintaan ini akan di Reject?</center></h5>

                    <center><button type="button" class="btn btn-danger" data-dismiss="modal" style="">
                        Tidak
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Yakin
                    </button></center>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach
    
@endsection